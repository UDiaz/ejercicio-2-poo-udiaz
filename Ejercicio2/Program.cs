﻿using System;
namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro miCarro = new Carro();

            miCarro.color = "Rojo";
            miCarro.marca = "Chevrolet";
            miCarro.numeroPuertas = 5;
            miCarro.anymodelo = 2013;
            miCarro.precio = 150000;
            miCarro.litrosIniciales = 40;
            miCarro.miMotor.asignarNumeroCilindros(4);
            miCarro.miMotor.asignarTipoCarburador("Inyeccion");
            miCarro.miMotor.TipoCombustible = "Gasolina";
            miCarro.obtenerCaracteristicas();
            miCarro.miTanque.llenarTanque((decimal)40.0+miCarro.litrosIniciales);
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
        }
    }
}
